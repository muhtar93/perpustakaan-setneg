package id.go.setneg.perpustakaan.perpustakaanonline;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import id.go.setneg.perpustakaan.perpustakaanonline.adapter.AgendaAdapter;
import id.go.setneg.perpustakaan.perpustakaanonline.config.ConfigURL;
import id.go.setneg.perpustakaan.perpustakaanonline.config.SharedPrefManager;
import id.go.setneg.perpustakaan.perpustakaanonline.model.Agenda;

public class AgendaFragment extends Fragment {
    private List<Agenda> agendaList = new ArrayList<>();
    private RecyclerView recyclerView;
    private AgendaAdapter agendaAdapter;
    private int requestCount = 0;

    public AgendaFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View itemView = inflater.inflate(R.layout.fragment_buku, container, false);
        recyclerView = (RecyclerView) itemView.findViewById(R.id.rv_catalog);

        agendaAdapter = new AgendaAdapter(agendaList, getActivity());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(agendaAdapter);

        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (isLastItemDisplaying(recyclerView)) {
                    requestCount = requestCount + 20;
                    getAgenda(String.valueOf(requestCount));
                }
            }
        });

        getAgenda(String.valueOf(requestCount));

        return itemView;
    }

    public void getAgenda(final String strOffset) {
        StringRequest request =
                new StringRequest(Request.Method.GET,
                        //ConfigURL.AGENDA + "?start_date=1489436056&end_date=1489695256?offset="+strOffset+"&limit=20",
                        ConfigURL.AGENDA + "?offset="+strOffset+"&limit=20",
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                showJSON(response);
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError volleyError) {
                                if (volleyError instanceof NetworkError) {
                                    Toast.makeText(getActivity(), "cek koneksi internet anda", Toast.LENGTH_SHORT).show();
                                } else if (volleyError instanceof ServerError) {
                                    Toast.makeText(getActivity(), "server tidak ditemukan", Toast.LENGTH_SHORT).show();
                                } else if (volleyError instanceof AuthFailureError) {
                                    Toast.makeText(getActivity(), "cek koneksi internet anda", Toast.LENGTH_SHORT).show();
                                } else if (volleyError instanceof ParseError) {
                                    Toast.makeText(getActivity(), "parsing data gagal", Toast.LENGTH_SHORT).show();
                                } else if (volleyError instanceof NoConnectionError) {
                                    Toast.makeText(getActivity(), "koneksi internet tidak ada", Toast.LENGTH_SHORT).show();
                                } else if (volleyError instanceof TimeoutError) {
                                    Toast.makeText(getActivity(), "koneksi timeout", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> headers = new HashMap<String, String>();
                        headers.put(ConfigURL.APP, SharedPrefManager.getInstance(getActivity()).getDeviceId());
                        headers.put(ConfigURL.TOKEN, SharedPrefManager.getInstance(getActivity()).getToken());
                        return headers;
                    }
                };

        request.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(request);
    }

    private void showJSON(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray jsonArrayData = jsonObject.getJSONArray(ConfigURL.DATA);
            for (int i = 0; i < jsonArrayData.length(); i++){
                JSONObject jsonObjectData = jsonArrayData.getJSONObject(i);
                Agenda agenda = new Agenda();

                String tglMulai = jsonObjectData.getString(ConfigURL.AGENDA_TANGGAL_MULAI).toString();
                SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd");
                SimpleDateFormat output = new SimpleDateFormat("dd MMM yyyy");

                try {
                    Date oneWayTripDate = input.parse(tglMulai);
                    output.format(oneWayTripDate);
                    agenda.setTanggalMulai(output.format(oneWayTripDate));
                    Log.d(ConfigURL.AGENDA_TANGGAL_MULAI, tglMulai +" = " + output.format(oneWayTripDate));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                String tglSelesai = jsonObjectData.getString(ConfigURL.AGENDA_TANGGAL_SELESAI).toString();
                if (tglSelesai.equals("null")){
                    agenda.setTanggalSelesai("");
                } else {
                    SimpleDateFormat input1 = new SimpleDateFormat("yyyy-MM-dd");
                    SimpleDateFormat output1 = new SimpleDateFormat("dd MMM yyyy");
                    try {
                        Date oneWayTripDate = input1.parse(tglSelesai);
                        output1.format(oneWayTripDate);
                        agenda.setTanggalSelesai(output1.format(oneWayTripDate));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

                agenda.setJamMulai(jsonObjectData.getString(ConfigURL.AGENDA_JAM_MULAI));
                agenda.setJamSelesai(jsonObjectData.getString(ConfigURL.AGENDA_JAM_SELESAI));
                agenda.setLokasi(jsonObjectData.getString(ConfigURL.AGENDA_LOKASI));
                agenda.setJudul(jsonObjectData.getString(ConfigURL.AGENDA_JUDUL));
                agenda.setDetail(jsonObjectData.getString(ConfigURL.AGENDA_DETAIL));

                agendaList.add(agenda);
            }
            agendaAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private boolean isLastItemDisplaying(RecyclerView recyclerView) {
        if (recyclerView.getAdapter().getItemCount() != 0) {
            int lastVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition();
            if (lastVisibleItemPosition != RecyclerView.NO_POSITION && lastVisibleItemPosition == recyclerView.getAdapter().getItemCount() - 1)
                return true;
        }
        return false;
    }
}
