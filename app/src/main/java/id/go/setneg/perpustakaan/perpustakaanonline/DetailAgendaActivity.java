package id.go.setneg.perpustakaan.perpustakaanonline;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import id.go.setneg.perpustakaan.perpustakaanonline.config.ConfigURL;

public class DetailAgendaActivity extends AppCompatActivity {
    private TextView textTitle, textTanggal, textWaktu, textTempat;
    private LinearLayout textDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_agenda);

        textTitle = (TextView) findViewById(R.id.textTitle);
        textTanggal = (TextView) findViewById(R.id.tanggal);
        textWaktu = (TextView) findViewById(R.id.waktu);
        textTempat = (TextView) findViewById(R.id.tempat);
        textDetail = (LinearLayout) findViewById(R.id.textContent);

        setTitle("Detail Agenda");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        textTitle.setText(intent.getStringExtra(ConfigURL.AGENDA_JUDUL));
        textTanggal.setText(intent.getStringExtra(ConfigURL.AGENDA_TANGGAL_MULAI) + " - "
        + intent.getStringExtra(ConfigURL.AGENDA_TANGGAL_SELESAI));
        textWaktu.setText(intent.getStringExtra(ConfigURL.AGENDA_JAM_MULAI) + " - "
        + intent.getStringExtra(ConfigURL.AGENDA_JAM_SELESAI));
        textTempat.setText(intent.getStringExtra(ConfigURL.AGENDA_LOKASI));

        Log.d("jam mulai", intent.getStringExtra(ConfigURL.AGENDA_JAM_MULAI));

        WebView view = new WebView(this);
        view.setVerticalScrollBarEnabled(false);
        textDetail.addView(view);
        view.loadData(intent.getStringExtra(ConfigURL.AGENDA_DETAIL), "text/html; charset=utf-8", "utf-8");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home){
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
