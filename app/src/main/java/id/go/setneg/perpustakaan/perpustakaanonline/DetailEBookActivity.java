package id.go.setneg.perpustakaan.perpustakaanonline;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import id.go.setneg.perpustakaan.perpustakaanonline.config.ConfigURL;

public class DetailEBookActivity extends AppCompatActivity {
    private TextView textTitle, textThnTerbit, textSinopsis, textPublisher, textSor, textDeskFisik, textIsbn, textPengarang;
    private Button btnDownload;
    private ImageView imageCover;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_ebook);

        textTitle = (TextView) findViewById(R.id.textTitle);
        textSinopsis = (TextView) findViewById(R.id.textSinopsis);
        textPublisher = (TextView) findViewById(R.id.textPenerbit);
        textThnTerbit = (TextView) findViewById(R.id.textThnTerbit);
        textIsbn = (TextView) findViewById(R.id.textISBN);
        textPengarang = (TextView) findViewById(R.id.textPengarang);
        btnDownload = (Button) findViewById(R.id.btnDownload);
        imageCover = (ImageView) findViewById(R.id.imageCover);

        final Intent intent = getIntent();
        textTitle.setText(intent.getStringExtra(ConfigURL.TITLE));
        textPublisher.setText(intent.getStringExtra(ConfigURL.PUBLISHER_NAME));
        textThnTerbit.setText(intent.getStringExtra(ConfigURL.PUBLISH_YEAR));
        textIsbn.setText(intent.getStringExtra(ConfigURL.ISBN_ISNN));
        textPengarang.setText(intent.getStringExtra(ConfigURL.AUTHOR_NAME));
        textSinopsis.setText(intent.getStringExtra(ConfigURL.NOTES));

        Glide.with(DetailEBookActivity.this).load(intent.getStringExtra(ConfigURL.IMAGE))
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.setneg)
                .into(imageCover);

        btnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (intent.getStringExtra(ConfigURL.FILES).equals("empty")){
                    Toast.makeText(DetailEBookActivity.this, "PDF Belum Tersedia", Toast.LENGTH_SHORT).show();
                } else {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(intent.getStringExtra(ConfigURL.FILES)));
                    startActivity(browserIntent);
                }
            }
        });

        setTitle("DETAIL EBOOK");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
