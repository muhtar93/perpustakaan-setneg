package id.go.setneg.perpustakaan.perpustakaanonline;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

public class KatalogFragment extends Fragment {

    public KatalogFragment() {
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.catalog:
                    CatalogFragment catalogFragment = new CatalogFragment();
                    FragmentTransaction fragmentTransaction1 = getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction1.replace(R.id.content, catalogFragment);
                    fragmentTransaction1.commit();
                    item.getTitle();
                    ((MainMenuActivity) getActivity())
                            .setActionBarTitle(item.getTitle().toString());
                    return true;
                case R.id.buku:
                    BukuFragment bukuFragment = new BukuFragment();
                    FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.content, bukuFragment);
                    item.getTitle();
                    fragmentTransaction.commit();
                    ((MainMenuActivity) getActivity())
                            .setActionBarTitle(item.getTitle().toString());
                    return true;
                case R.id.local_repository:
                    LocalRepositoryFragment localRepositoryFragment = new LocalRepositoryFragment();
                    FragmentTransaction fragmentTransaction2 = getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction2.replace(R.id.content, localRepositoryFragment);
                    fragmentTransaction2.commit();
                    ((MainMenuActivity) getActivity())
                            .setActionBarTitle(item.getTitle().toString());
                    return true;
                default:
                    CatalogFragment catalogFragment1 = new CatalogFragment();
                    FragmentTransaction fragmentTransaction4 = getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction4.replace(R.id.content, catalogFragment1);
                    fragmentTransaction4.commit();
                    ((MainMenuActivity) getActivity())
                            .setActionBarTitle(item.getTitle().toString());
            }
            return false;
        }

    };


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View itemView = inflater.inflate(R.layout.activity_main, container, false);
        BottomNavigationView navigation = (BottomNavigationView) itemView.findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        CatalogFragment catalogFragment = new CatalogFragment();
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content, catalogFragment);
        fragmentTransaction.commit();
        ((MainMenuActivity) getActivity())
                .setActionBarTitle("KATALOG");

        return itemView;
    }

}
