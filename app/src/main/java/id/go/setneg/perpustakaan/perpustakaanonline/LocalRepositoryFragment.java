package id.go.setneg.perpustakaan.perpustakaanonline;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import id.go.setneg.perpustakaan.perpustakaanonline.adapter.CatalogAdapter;
import id.go.setneg.perpustakaan.perpustakaanonline.config.ConfigURL;
import id.go.setneg.perpustakaan.perpustakaanonline.config.SharedPrefManager;
import id.go.setneg.perpustakaan.perpustakaanonline.model.Catalog;

public class LocalRepositoryFragment extends Fragment {
    private List<Catalog> catalogList = new ArrayList<>();
    private RecyclerView recyclerView;
    private CatalogAdapter catalogAdapter;
    private ProgressDialog progressDialog;
    private int requestCount = 0;

    public LocalRepositoryFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View itemView = inflater.inflate(R.layout.fragment_buku, container, false);
        recyclerView = (RecyclerView) itemView.findViewById(R.id.rv_catalog);

        catalogAdapter = new CatalogAdapter(catalogList, getActivity());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(catalogAdapter);

        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (isLastItemDisplaying(recyclerView)) {
                    requestCount = requestCount + 20;
                    getEBook(String.valueOf(requestCount));
                }
            }
        });

        getEBook(String.valueOf(requestCount));

        return itemView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please Wait...");
        progressDialog.show();
    }

    public void getEBook(final String strOffset){
        StringRequest request = new StringRequest(Request.Method.GET, ConfigURL.LOCAL_REPO+"?offset="+strOffset+"&limit=20",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        showJSON(response);
                        progressDialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        if (volleyError instanceof NetworkError){
                            Toast.makeText(getActivity(), ConfigURL.CEK_KONEKSI, Toast.LENGTH_SHORT).show();
                        } else if (volleyError instanceof ServerError){
                            Toast.makeText(getActivity(), ConfigURL.SERVER_NOT_FOUND, Toast.LENGTH_SHORT).show();
                        } else if (volleyError instanceof AuthFailureError){
                            Toast.makeText(getActivity(), ConfigURL.CEK_KONEKSI, Toast.LENGTH_SHORT).show();
                        } else if (volleyError instanceof ParseError){
                            Toast.makeText(getActivity(), ConfigURL.PARSING_DATA, Toast.LENGTH_SHORT).show();
                        } else if (volleyError instanceof NoConnectionError){
                            Toast.makeText(getActivity(), ConfigURL.CONNECTION_NOT_FOUND, Toast.LENGTH_SHORT).show();
                        } else if (volleyError instanceof TimeoutError){
                            Toast.makeText(getActivity(), ConfigURL.KONEKSI_TIMEOUT, Toast.LENGTH_SHORT).show();
                        }
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> headers = new HashMap<String, String>();
                headers.put(ConfigURL.APP, SharedPrefManager.getInstance(getActivity()).getDeviceId());
                headers.put(ConfigURL.TOKEN, SharedPrefManager.getInstance(getActivity()).getToken());
                return headers;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                12000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(request);
    }

    public void showJSON(String response){
        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray jsonArrayData = jsonObject.getJSONArray(ConfigURL.DATA);
            for (int i = 0; i < jsonArrayData.length(); i++){
                JSONObject jsonObjectData = jsonArrayData.getJSONObject(i);
                Catalog catalog = new Catalog();
                catalog.setJudul(jsonObjectData.getString(ConfigURL.TITLE));
                catalog.setImage(jsonObjectData.getString(ConfigURL.IMAGE));
                catalog.setThnTerbit(jsonObjectData.getString(ConfigURL.PUBLISH_YEAR));
                catalog.setBahasa(jsonObjectData.getString(ConfigURL.LANGUAGE_ID));
                catalog.setSinopsis(jsonObjectData.getString(ConfigURL.NOTES));
                if (jsonObjectData.getString(ConfigURL.SOR).equals("null")){
                    catalog.setSor("");
                } else {
                    catalog.setSor(jsonObjectData.getString(ConfigURL.SOR));
                }
                catalog.setJmlHalaman(jsonObjectData.getString(ConfigURL.COLLATION));
                catalog.setIsbn(jsonObjectData.getString(ConfigURL.ISBN_ISNN));
                catalogList.add(catalog);

                if (jsonObjectData.getString(ConfigURL.PUBLISHER) == "false"){
                    catalog.setPublisher(jsonObjectData.getString(ConfigURL.PUBLISHER));
                } else {
                    JSONObject jsonObjectPublisher = jsonObjectData.getJSONObject(ConfigURL.PUBLISHER);
                    catalog.setPublisher(jsonObjectPublisher.getString(ConfigURL.PUBLISHER_NAME));
                    Log.d(ConfigURL.PUBLISHER, ""+jsonObjectPublisher.getString(ConfigURL.PUBLISHER_NAME));
                }

                JSONArray jsonArrayAuthor = jsonObjectData.getJSONArray(ConfigURL.AUTHOR);
                if (jsonArrayAuthor.isNull(0)){
                    catalog.setAuthor("-");
                } else {
                    for (int j = 0; j < jsonArrayAuthor.length(); j++) {
                        JSONObject jsonObjectAuthor = jsonArrayAuthor.getJSONObject(j);
                        catalog.setAuthor(jsonObjectAuthor.getString(ConfigURL.AUTHOR_NAME));
                    }
                    Log.d("author", jsonArrayAuthor.toString());
                }

                JSONArray jsonArrayFiles = jsonObjectData.getJSONArray(ConfigURL.FILES);
                if(jsonArrayFiles != null && jsonArrayFiles.length() > 0 ){
                    for (int k = 0; k < jsonArrayFiles.length(); k++){
                        JSONObject jsonObjectFiles = jsonArrayFiles.getJSONObject(k);
                        catalog.setPdf(jsonObjectFiles.getString(ConfigURL.URL));
                    }
                } else {
                    catalog.setPdf("empty");
                }
            }
            catalogAdapter.notifyDataSetChanged();
        } catch (JSONException e){
            e.printStackTrace();
            Log.e("error json", e.toString());
        }
    }

    private boolean isLastItemDisplaying(RecyclerView recyclerView) {
        if (recyclerView.getAdapter().getItemCount() != 0) {
            int lastVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition();
            if (lastVisibleItemPosition != RecyclerView.NO_POSITION && lastVisibleItemPosition == recyclerView.getAdapter().getItemCount() - 1)
                return true;
        }
        return false;
    }

}
