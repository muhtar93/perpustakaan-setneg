package id.go.setneg.perpustakaan.perpustakaanonline;

import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import id.go.setneg.perpustakaan.perpustakaanonline.config.ConfigURL;
import id.go.setneg.perpustakaan.perpustakaanonline.config.SharedPrefManager;

import static id.go.setneg.perpustakaan.perpustakaanonline.config.ConfigURL.TAG_KATALOG;
import static id.go.setneg.perpustakaan.perpustakaanonline.config.ConfigURL.TAG_KONTAK;
import static id.go.setneg.perpustakaan.perpustakaanonline.config.ConfigURL.TAG_SETNEGLIB;

public class MainMenuActivity extends AppCompatActivity {
    private NavigationView navigationView;
    private DrawerLayout drawer;
    private Toolbar toolbar;
    public static int navItemIndex = 0;
    public static String CURRENT_TAG = TAG_KATALOG;
    private String[] activityTitles;
    private boolean shouldLoadHomeFragOnBackPress = true;
    private Handler mHandler;
    private String strToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mHandler = new Handler();
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        activityTitles = getResources().getStringArray(R.array.nav_item_activity_titles);

        setUpNavigationView();

        if (savedInstanceState == null) {
            navItemIndex = 0;
            CURRENT_TAG = TAG_KATALOG;
            loadHomeFragment();
        }

        getToken();

        String strDeviceId = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        if (SharedPrefManager.getInstance(this).saveDeviceId(strDeviceId)){
            strDeviceId = SharedPrefManager.getInstance(this).getDeviceId();
            Log.d(ConfigURL.DEVICE_ID, strDeviceId);
        } else {
            strDeviceId = ConfigURL.DEVICE_ID_NOT_FOUND;
            Log.d(ConfigURL.DEVICE_ID, strDeviceId);
        }
    }

    private void loadHomeFragment() {
        selectNavMenu();
        setToolbarTitle();

        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            drawer.closeDrawers();
            return;
        }

        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                Fragment fragment = getHomeFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                fragmentTransaction.commitAllowingStateLoss();
            }
        };

        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }

        drawer.closeDrawers();
        invalidateOptionsMenu();
    }

    private Fragment getHomeFragment() {
        switch (navItemIndex) {
            case 0:
                KatalogFragment katalogFragment = new KatalogFragment();
                return katalogFragment;
            case 1:
                AgendaFragment agendaFragment = new AgendaFragment();
                return agendaFragment;
            case 2:
                KontakFragment kontakFragment = new KontakFragment();
                return kontakFragment;
            default:
                return new KatalogFragment();
        }
    }

    public void setActionBarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    private void setToolbarTitle() {
        getSupportActionBar().setTitle(activityTitles[navItemIndex]);
    }

    private void selectNavMenu() {
        navigationView.getMenu().getItem(navItemIndex).setChecked(true);
    }

    private void setUpNavigationView() {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.nav_katalog:
                        navItemIndex = 0;
                        CURRENT_TAG = TAG_KATALOG;
                        break;
                    case R.id.nav_agenda:
                        navItemIndex = 1;
                        CURRENT_TAG = ConfigURL.TAG_AGENDA;
                        break;
                    case R.id.nav_kontak:
                        navItemIndex = 2;
                        CURRENT_TAG = TAG_KONTAK;
                        break;
                    default:
                        navItemIndex = 0;
                }

                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }
                menuItem.setChecked(true);

                loadHomeFragment();
                return true;
            }
        });


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.open,
                R.string.close) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        drawer.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        }

        if (shouldLoadHomeFragOnBackPress) {
            if (navItemIndex != 0) {
                navItemIndex = 0;
                CURRENT_TAG = TAG_SETNEGLIB;
                loadHomeFragment();
                return;
            }
        }

        super.onBackPressed();
    }

    public void getToken() {
        StringRequest request =
                new StringRequest(Request.Method.POST,
                        ConfigURL.AUTH,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                showJSON(response);
                                //Log.d("response",response);
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError volleyError) {
                                if (volleyError instanceof NetworkError){
                                    Toast.makeText(MainMenuActivity.this, ConfigURL.CEK_KONEKSI, Toast.LENGTH_SHORT).show();
                                } else if (volleyError instanceof ServerError){
                                    Toast.makeText(MainMenuActivity.this, ConfigURL.SERVER_NOT_FOUND, Toast.LENGTH_SHORT).show();
                                } else if (volleyError instanceof AuthFailureError){
                                    Toast.makeText(MainMenuActivity.this, ConfigURL.CEK_KONEKSI, Toast.LENGTH_SHORT).show();
                                } else if (volleyError instanceof ParseError){
                                    Toast.makeText(MainMenuActivity.this, ConfigURL.PARSING_DATA, Toast.LENGTH_SHORT).show();
                                } else if (volleyError instanceof NoConnectionError){
                                    Toast.makeText(MainMenuActivity.this, ConfigURL.CONNECTION_NOT_FOUND, Toast.LENGTH_SHORT).show();
                                } else if (volleyError instanceof TimeoutError){
                                    Toast.makeText(MainMenuActivity.this, ConfigURL.KONEKSI_TIMEOUT, Toast.LENGTH_SHORT).show();
                                }
                            }
                        }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put(ConfigURL.P, ConfigURL.PASSWORD);
                        params.put(ConfigURL.APP_ID, SharedPrefManager.getInstance(MainMenuActivity.this).getDeviceId());
                        return params;
                    }
                };

        request.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(MainMenuActivity.this);
        requestQueue.add(request);
    }

    private String showJSON(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONObject jsonObjectToken = jsonObject.getJSONObject(ConfigURL.DATA);
            strToken = jsonObjectToken.getString(ConfigURL.TOKEN).toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (SharedPrefManager.getInstance(this).saveToken(strToken)){
            strToken = SharedPrefManager.getInstance(this).getToken();
            Log.d(ConfigURL.TOKEN, strToken);
        } else {
            strToken = ConfigURL.TOKEN_NOT_FOUND;
            Log.d(ConfigURL.TOKEN, strToken);
        }

        return strToken;
    }
}
