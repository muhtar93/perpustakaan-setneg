package id.go.setneg.perpustakaan.perpustakaanonline.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import id.go.setneg.perpustakaan.perpustakaanonline.DetailAgendaActivity;
import id.go.setneg.perpustakaan.perpustakaanonline.R;
import id.go.setneg.perpustakaan.perpustakaanonline.config.ConfigURL;
import id.go.setneg.perpustakaan.perpustakaanonline.listener.ItemClickListener;
import id.go.setneg.perpustakaan.perpustakaanonline.model.Agenda;

/**
 * Created by Muhtar on 23/05/2017.
 */

public class AgendaAdapter extends RecyclerView.Adapter<AgendaAdapter.ViewHolder> {
    private List<Agenda> agendaList;
    private Context context;

    public AgendaAdapter(List<Agenda> agendaList, Context context) {
        this.agendaList = agendaList;
        this.context = context;
    }

    @Override
    public AgendaAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.new_agenda_list_row, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AgendaAdapter.ViewHolder holder, int position) {
        final Agenda agenda = agendaList.get(position);
        holder.tanggal.setText(agenda.getTanggalMulai()+" - "+agenda.getTanggalSelesai());
        holder.agenda.setText(agenda.getJamMulai()+"-"+agenda.getJudul());
        holder.setClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view) {
                Intent detailAgenda = new Intent(context, DetailAgendaActivity.class);
                detailAgenda.putExtra(ConfigURL.AGENDA_JUDUL, agenda.getJudul());
                detailAgenda.putExtra(ConfigURL.AGENDA_DETAIL, agenda.getDetail());
                detailAgenda.putExtra(ConfigURL.AGENDA_JAM_MULAI, agenda.getJamMulai());
                detailAgenda.putExtra(ConfigURL.AGENDA_JAM_SELESAI, agenda.getJamSelesai());
                detailAgenda.putExtra(ConfigURL.AGENDA_TANGGAL_MULAI, agenda.getTanggalMulai());
                detailAgenda.putExtra(ConfigURL.AGENDA_TANGGAL_SELESAI, agenda.getTanggalSelesai());
                detailAgenda.putExtra(ConfigURL.AGENDA_LOKASI, agenda.getLokasi());
                context.startActivity(detailAgenda);
            }
        });
    }

    @Override
    public int getItemCount() {
        return agendaList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView tanggal, agenda;
        private ItemClickListener clickListener;

        public ViewHolder(View itemView) {
            super(itemView);

            tanggal = (TextView)itemView.findViewById(R.id.tanggal);
            agenda = (TextView)itemView.findViewById(R.id.agenda);

            itemView.setOnClickListener(this);
        }

        public void setClickListener(ItemClickListener itemClickListener) {
            this.clickListener = itemClickListener;
        }

        @Override
        public void onClick(View view) {
            clickListener.onClick(view);
        }
    }
}
