package id.go.setneg.perpustakaan.perpustakaanonline.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import id.go.setneg.perpustakaan.perpustakaanonline.DetailEBookActivity;
import id.go.setneg.perpustakaan.perpustakaanonline.R;
import id.go.setneg.perpustakaan.perpustakaanonline.config.ConfigURL;
import id.go.setneg.perpustakaan.perpustakaanonline.listener.ItemClickListener;
import id.go.setneg.perpustakaan.perpustakaanonline.model.Catalog;

/**
 * Created by Muhtar on 23/05/2017.
 */

public class CatalogAdapter extends RecyclerView.Adapter<CatalogAdapter.ViewHolder> {
    private List<Catalog> catalogList;
    private Context context;

    public CatalogAdapter(List<Catalog> catalogList, Context context) {
        this.catalogList = catalogList;
        this.context = context;
    }

    @Override
    public CatalogAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.catalog_list_row, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CatalogAdapter.ViewHolder holder, int position) {
        final Catalog catalog = catalogList.get(position);
        holder.judul.setText(catalog.getJudul()+"/"+catalog.getAuthor());
        holder.setClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view) {
                Intent detailEBook = new Intent(context, DetailEBookActivity.class);
                detailEBook.putExtra(ConfigURL.TITLE, catalog.getJudul());
                detailEBook.putExtra(ConfigURL.IMAGE, catalog.getImage());
                detailEBook.putExtra(ConfigURL.PUBLISH_YEAR, catalog.getThnTerbit());
                detailEBook.putExtra(ConfigURL.LANGUAGE_ID, catalog.getBahasa());
                detailEBook.putExtra(ConfigURL.PUBLISHER_NAME, catalog.getPublisher());
                detailEBook.putExtra(ConfigURL.SOR, catalog.getSor());
                detailEBook.putExtra(ConfigURL.COLLATION, catalog.getJmlHalaman());
                detailEBook.putExtra(ConfigURL.ISBN_ISNN, catalog.getIsbn());
                detailEBook.putExtra(ConfigURL.AUTHOR_NAME, catalog.getAuthor());
                detailEBook.putExtra(ConfigURL.FILES, catalog.getPdf());
                detailEBook.putExtra(ConfigURL.NOTES, catalog.getSinopsis());
                context.startActivity(detailEBook);
            }
        });

        Glide.with(context).load(catalog.getImage())
                .thumbnail(0.5f)
                .crossFade()
                .error(R.drawable.setneg)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.image);
    }

    @Override
    public int getItemCount() {
        return catalogList == null ? 0 : catalogList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView judul, pengarang;
        public ImageView image;
        public ItemClickListener itemClickListener;

        public ViewHolder(View itemView) {
            super(itemView);

            judul = (TextView)itemView.findViewById(R.id.judul);
            pengarang = (TextView)itemView.findViewById(R.id.pengarang);
            image = (ImageView)itemView.findViewById(R.id.image);

            itemView.setOnClickListener(this);
        }

        public void setClickListener(ItemClickListener itemClickListener){
            this.itemClickListener = itemClickListener;
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onClick(view);
        }
    }
}
