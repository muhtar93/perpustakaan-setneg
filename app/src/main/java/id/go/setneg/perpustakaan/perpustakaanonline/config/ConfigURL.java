package id.go.setneg.perpustakaan.perpustakaanonline.config;

/**
 * Created by Muhtar on 26/10/2016.
 */

public class ConfigURL {
    //url
    public static final String BASE_URL = "https://perpustakaan.setneg.go.id/api/";
    //public static final String BASE_URL = "http://api.perpustakaan.lumpat.web.id/";
    public static final String AUTH = BASE_URL+"auth/get_token";
    public static final String AGENDA = BASE_URL+"agenda";
    public static final String BOOK = BASE_URL+"book/ebook";
    public static final String CATALOG = BASE_URL+"book/catalog";
    public static final String LOCAL_REPO = BASE_URL+"book/localrepo";

    //menu
    public static final String TAG_SETNEGLIB = "SETNEGLIB";
    public static final String TAG_KATALOG = "KATALOG";
    public static final String TAG_AGENDA = "AGENDA";
    public static final String TAG_KONTAK = "KONTAK";

    //param
    public static final String OFFSET = "offset";
    public static final String LIMIT = "limit";
    public static final String APP = "app";
    public static final String P = "p";
    public static final String APP_ID = "app_id";
    public static final String TOKEN = "token";
    public static final String PASSWORD = "ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413";

    //key
    public static final String DATA = "data";
    public static final String AGENDA_TANGGAL_MULAI = "agenda_tanggal_mulai";
    public static final String AGENDA_TANGGAL_SELESAI = "agenda_tanggal_selesai";
    public static final String AGENDA_JAM_MULAI = "agenda_jam_mulai";
    public static final String AGENDA_JAM_SELESAI = "agenda_jam_selesai";
    public static final String AGENDA_LOKASI = "agenda_lokasi";
    public static final String AGENDA_JUDUL = "agenda_judul";
    public static final String AGENDA_DETAIL = "agenda_detail";
    public static final String TITLE = "title";
    public static final String IMAGE = "image";
    public static final String DEVICE_ID = "device id";
    public static final String PUBLISHER = "publisher";
    public static final String PUBLISHER_NAME = "publisher_name";
    public static final String COLLATION = "collation";
    public static final String PUBLISH_YEAR = "publish_year";
    public static final String LANGUAGE_ID = "language_id";
    public static final String SOR = "sor";
    public static final String ISBN_ISNN = "isbn_issn";
    public static final String AUTHOR = "author";
    public static final String AUTHOR_NAME = "author_name";
    public static final String FILES = "files";
    public static final String URL = "url";
    public static final String NOTES = "notes";

    //note
    public static final String DEVICE_ID_NOT_FOUND = "device id not found";
    public static final String CEK_KONEKSI = "cek koneksi internet anda";
    public static final String SERVER_NOT_FOUND = "server tidak ditemukan";
    public static final String PARSING_DATA = "parsing data gagal";
    public static final String CONNECTION_NOT_FOUND = "koneksi internet tidak ada";
    public static final String KONEKSI_TIMEOUT = "koneksi timeout";
    public static final String TOKEN_NOT_FOUND = "token not found";
}
