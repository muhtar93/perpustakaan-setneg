package id.go.setneg.perpustakaan.perpustakaanonline.config;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Muhtar on 05/06/2017.
 */

public class SharedPrefManager {
    private static final String SHARED_PREF_NAME = "TokenSharedPref";
    private static SharedPrefManager sharedPrefManager;
    private static Context context;

    public SharedPrefManager(Context context) {
        this.context = context;
    }

    public static synchronized SharedPrefManager getInstance(Context context) {
        if (sharedPrefManager == null) {
            sharedPrefManager = new SharedPrefManager(context);
        }

        return sharedPrefManager;
    }

    public boolean saveToken(String strToken){
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(ConfigURL.TOKEN, strToken);
        editor.apply();
        return true;
    }

    public boolean saveDeviceId(String strDeviceId){
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(ConfigURL.APP, strDeviceId);
        editor.apply();
        return true;
    }

    public String getToken(){
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(ConfigURL.TOKEN, null);
    }

    public String getDeviceId(){
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(ConfigURL.APP, null);
    }
}
