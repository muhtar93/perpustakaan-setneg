package id.go.setneg.perpustakaan.perpustakaanonline.model;

/**
 * Created by Muhtar on 23/05/2017.
 */

public class Catalog {
    public String judul;
    public String image;
    public String publisher;
    public String jmlHalaman;
    public String bahasa;
    public String thnTerbit;
    public String sor;
    public String isbn;
    public String author;
    public String pdf;
    public String sinopsis;

    public Catalog() {
    }

    public Catalog(String judul, String image, String publisher, String jmlHalaman, String bahasa) {
        this.judul = judul;
        this.image = image;
        this.publisher = publisher;
        this.jmlHalaman = jmlHalaman;
        this.bahasa = bahasa;
    }

    public String getSinopsis() {
        return sinopsis;
    }

    public void setSinopsis(String sinopsis) {
        this.sinopsis = sinopsis;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getJmlHalaman() {
        return jmlHalaman;
    }

    public void setJmlHalaman(String jmlHalaman) {
        this.jmlHalaman = jmlHalaman;
    }

    public String getBahasa() {
        return bahasa;
    }

    public void setBahasa(String bahasa) {
        this.bahasa = bahasa;
    }

    public String getThnTerbit() {
        return thnTerbit;
    }

    public void setThnTerbit(String thnTerbit) {
        this.thnTerbit = thnTerbit;
    }

    public String getSor() {
        return sor;
    }

    public void setSor(String sor) {
        this.sor = sor;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPdf() {
        return pdf;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }
}
